package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    int random;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random r = new Random();
        int low = 15;
        int high = 20;
        random = r.nextInt(high-low) + low;
    }

    @Override
    public String getName() {

        return "Magic " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        return (weapon != null ? weapon.getWeaponValue() : 0) + random;
        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "ini adalah Magic " + weapon.getDescription();
        //TODO: Complete me
    }
}
