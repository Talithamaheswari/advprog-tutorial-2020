package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;
    int random;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random r = new Random();
        int low = 50;
        int high = 55;
        random = r.nextInt(high-low) + low;
    }

    @Override
    public String getName() {

        return "Chaos " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 50-55 ++
    @Override
    public int getWeaponValue() {
        return (weapon != null ? weapon.getWeaponValue() : 0) + random;
        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "ini adalah Chaos " + weapon.getDescription();
        //TODO: Complete me
    }
}
