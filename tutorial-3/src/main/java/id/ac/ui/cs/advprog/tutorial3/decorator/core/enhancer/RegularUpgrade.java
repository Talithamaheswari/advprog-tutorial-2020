package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;
    int random;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random r = new Random();
        int low = 1;
        int high = 5;
        random = r.nextInt(high-low) + low;
    }

    @Override
    public String getName() {

        return "Regular " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        return (weapon != null ? weapon.getWeaponValue() : 0) + random;
        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "ini adalah Regular " + weapon.getDescription();
        //TODO: Complete me
    }
}
