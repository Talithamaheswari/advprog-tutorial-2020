package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    int random;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
        Random r = new Random();
        int low = 10;
        int high = 15;
        random = r.nextInt(high-low) + low;
    }

    @Override
    public String getName() {

        return "Unique " + weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        return (weapon != null ? weapon.getWeaponValue() : 0) + random;
        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "ini adalah Unique " + weapon.getDescription();
        //TODO: Complete me
    }
}
