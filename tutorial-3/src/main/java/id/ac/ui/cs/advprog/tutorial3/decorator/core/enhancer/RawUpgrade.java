package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    int random;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random r = new Random();
        int low = 5;
        int high = 10;
        random = r.nextInt(high-low) + low;
    }

    @Override
    public String getName() {

        return "Raw " + weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        return (weapon != null ? weapon.getWeaponValue() : 0) + random;
        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "ini adalah Raw " + weapon.getDescription();
        //TODO: Complete me
    }
}
