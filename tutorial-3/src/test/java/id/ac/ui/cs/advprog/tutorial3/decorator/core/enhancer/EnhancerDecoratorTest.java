package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        //TODO: Complete me

        int value1 = weapon3.getWeaponValue();
        assertTrue(value1>=1 && value1<=5);
        int value2 = weapon4.getWeaponValue();
        assertTrue(value2>=5 && value2<=10);
        int value3 = weapon5.getWeaponValue();
        assertTrue(value3>=10 && value3<=15);
        int value4 = weapon2.getWeaponValue();
        assertTrue(value4 >=15 && value4<=20);
        int value5 = weapon1.getWeaponValue();
        assertTrue(value5>=50 && value5<=55);
    }

}
