package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;

    @BeforeEach
    public void setUp(){
        regularUpgrade = new RegularUpgrade(new Sword());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Regular Sword", regularUpgrade.getName());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals("ini adalah Regular Sword", regularUpgrade.getDescription());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponValue(){
        int value = regularUpgrade.getWeaponValue();
        assertTrue(value >=26 && value <=30);
        //TODO: Complete me
    }
}
