package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;

    @BeforeEach
    public void setUp(){
        rawUpgrade = new RawUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals("Raw Shield", rawUpgrade.getName()) ;
        //TODO: Complete me
    }

    @Test
    public void testMethodGetDescription(){
        assertEquals("ini adalah Raw Shield", rawUpgrade.getDescription());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponValue(){
        int value = rawUpgrade.getWeaponValue();
        assertTrue(value >=15 && value <=20);
        //TODO: Complete me
    }
}
