package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Wati", member.getName());
        //TODO: Complete me
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Gold Merchant", member.getRole());
        //TODO: Complete me
    }

    @Test
    public void testMethodAddChildMember() {
        Member test = new OrdinaryMember("talitha", "luthfiyah");
        member.addChildMember(test);
        assertEquals(1, member.getChildMembers().size());
        //TODO: Complete me
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member test = new OrdinaryMember("talitha", "luthfiyah");
        member.addChildMember(test);
        member.removeChildMember(test);
        assertEquals(0, member.getChildMembers().size());
        //TODO: Complete me
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        member.addChildMember(new OrdinaryMember("talitha", "luthfiyah"));
        member.addChildMember(new OrdinaryMember("talitha", "luthfiyah"));
        member.addChildMember(new OrdinaryMember("talitha", "luthfiyah"));
        member.addChildMember(new OrdinaryMember("talitha", "luthfiyah"));
        assertEquals(3, member.getChildMembers().size());
        //TODO: Complete me
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member test = new PremiumMember("talitha", "Master");
        test.addChildMember(new OrdinaryMember("talitha", "luthfiyah"));
        test.addChildMember(new OrdinaryMember("talitha", "luthfiyah"));
        test.addChildMember(new OrdinaryMember("talitha", "luthfiyah"));
        test.addChildMember(new OrdinaryMember("talitha", "luthfiyah"));
        assertEquals(4, test.getChildMembers().size());
        //TODO: Complete me
    }
}
