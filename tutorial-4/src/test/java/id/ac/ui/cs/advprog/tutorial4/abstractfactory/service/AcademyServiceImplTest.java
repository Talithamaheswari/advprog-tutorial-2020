package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @Mock
    private AcademyService academyService;

    // TODO create tests

//    void produceKnight(String academyName, String knightType);
//    List<KnightAcademy> getKnightAcademies();
//    Knight getKnight();

    @BeforeEach
    public void setUp(){
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
    }

    @Test
    public void testProduceKnight(){
        academyService = new AcademyServiceImpl(new AcademyRepository());
        assertThat(academyService.getKnightAcademies().size()).isNotEqualTo(0);

        academyService.produceKnight("Lordran", "majestic");

        assertTrue(academyService.getKnight() instanceof MajesticKnight);
    }

    @Test
    public void testGetKnightAcademies(){

        assertEquals(2, academyService.getKnightAcademies().size());
    }

}
