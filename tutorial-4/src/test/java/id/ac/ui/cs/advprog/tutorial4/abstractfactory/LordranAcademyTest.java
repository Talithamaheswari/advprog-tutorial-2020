package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.DrangleicArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.LordranArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        lordranAcademy = new LordranAcademy();
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");
        // TODO setup me
    }

    @Test
    public void checkKnightInstances() {
        assertNotNull(majesticKnight);
        assertNotNull(metalClusterKnight);
        assertNotNull(syntheticKnight);
        // TODO create test
    }

    @Test
    public void checkKnightNames() {
        assertEquals("Lordran Majestic Knight", majesticKnight.getName());
        assertEquals("Lordran Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Lordran Synthetic Knight", syntheticKnight.getName());
        // TODO create test
    }

    @Test
    public void checkKnightDescriptions() {
        assertEquals("spesialisasi pada armor dan weapon", majesticKnight.getDescription());
        assertEquals("spesialisasi pada armor dan skill", metalClusterKnight.getDescription());
        assertEquals("spesialisasi pada weapon dan skill", syntheticKnight.getDescription());
        // TODO create test
    }
}
