package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MetalClusterKnight extends Knight {

    public MetalClusterKnight(Armory armory) {
        this.armory = armory;
        name = "Metal Cluster Knight";
        switch (armory.getClass().getSimpleName()){
            case "DrangleicArmory":
                name = "Drangleic "+name;
                break;
            case "LordranArmory":
                name = "Lordran "+name;
                break;
        }
    }

    @Override
    public void prepare() {
        // TODO complete me
        armor = armory.craftArmor();
        skill = armory.learnSkill();
    }

    @Override
    public String getDescription(){

        return "spesialisasi pada armor dan skill";
    }
}
