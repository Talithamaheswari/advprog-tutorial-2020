package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class SyntheticKnight extends Knight {

    public SyntheticKnight(Armory armory) {
        this.armory = armory;
        name = "Synthetic Knight";
        switch (armory.getClass().getSimpleName()){
            case "DrangleicArmory":
                name = "Drangleic "+name;
                break;
            case "LordranArmory":
                name = "Lordran "+name;
                break;
        }
    }

    @Override
    public void prepare() {
        weapon = armory.craftWeapon();
        skill = armory.learnSkill();
        // TODO complete me
    }

    @Override
    public String getDescription(){
        return "spesialisasi pada weapon dan skill";
    }
}
